#!/bin/bash
directfile="https://gitlab.com/tecbs/install_dwm_arch/-/raw/main/"
archivocsv="programas.csv"
srcdir="/home/$USER/.local/src"
bindir="/home/$USER/.local/bin"
configdir="/home/$USER/.config"
installdir="/home/$USER/instalador"
progsfile="$installdir/$archivocsv"
version="dwm-6.4-tecbs"

[ -d "$srcdir" ] && echo "$srcdir existe" || mkdir -p "$srcdir" || echo "$srcdir creado"
[ -d "$bindir" ] && echo "$bindir existe" || mkdir -p "$bindir" || echo "$bindir creado"
[ -d "$configdir" ] & echo "$configdir existe" || mkdir -p "$configdir" || echo "$configdir creado"
[ -d "$installdir" ] && rm -rf "$installdir" && echo "$installdir borrado"
[ -d "$installdir" ] || mkdir -p "$installdir" && echo "$installdir creado"

#instalar programas pacman del csv
sudo pacman --noconfirm -Syu
cd $installdir
wget $directfile$archivocsv
awk -F , '$1 == ""' $progsfile | cut -d "," -f2 | xargs sudo pacman --noconfirm --needed -S

#instalar yay
cd $srcdir
[ -d "$srcdir/yay" ] && rm -rf "$srcdir/yay"
git clone https://aur.archlinux.org/yay.git
sudo chown -R  $USER:$USER yay
cd yay
makepkg -si --noconfirm
yay --version

#instalar programas yay
awk -F, '$1 == "A"' $progsfile | cut -d "," -f2 | xargs yay --noconfirm -S

#instalar programas pip
awk -F, '$1 == "PIP"' $progsfile | cut -d "," -f2 | xargs pip3 install


#instalar dwm

cd $srcdir
[ -d "$srcdir/dwm" ]  && rm -rf "$srcdir/dwm"
[ -d "$srcdir/dmenu" ] && rm -rf "$srcdir/dmenu"
[ -d "$srcdir/st" ] && rm -rf "$srcdir/st"
[ -d "$srcdir/$verion" ] && rm -rf "$srcdir/$version"
git clone https://gitlab.com/tecbs/$version.git
git clone https://git.suckless.org/dmenu
git clone https://gitlab.com/tecbs/st.git
mv $version/ dwm/
cd $srcdir/dwm
make
cd $srcdir/dmenu
make
cd $srcdir/st
make
cd $bindir
[ -L "$bindir/dwm" ] && rm "$bindir/dwm" 
[ -L "$bindir/dmenu" ] && rm "$bindir/dmenu" 
[ -L "$bindir/dmenu_run" ] && rm "$bindir/dmenu_run" 
[ -L "$bindir/dmenu_path" ] && rm "$bindir/dmenu_path" 
[ -L "$bindir/stest" ] && rm "$bindir/stest" 
[ -L "$bindir/st" ] && rm "$bindir/st" 
ln -s $srcdir/dwm/dwm
ln -s $srcdir/dmenu/dmenu
ln -s $srcdir/dmenu/dmenu_run
ln -s $srcdir/dmenu/dmenu_path
ln -s $srcdir/dmenu/stest
ln -s $srcdir/st/st
cd ~

#configuracion de los dotfiles
chezmoi init http://gitlab.com/tecbs/dotfiles_chezmoi.git
chezmoi -v apply

#programas necesarios para nvim
git clone https://github.com/LunarVim/Neovim-from-scratch.git ~/.config/nvim


#applicar zsh
chsh -s /bin/zsh

echo "reinicia la sesion para que los cambios zsh tengan efecto"





